from django.contrib import admin
from .models import ExpenseCategory, Receipt, Account

# Register your models here.
# SHOULD THESE ALL INCLUDE ID?
@admin.register(ExpenseCategory)
class ExpenseCategoryAdmin(admin.ModelAdmin):
    list_display = ("name", "id")


@admin.register(Receipt)
class ReceiptAdmin(admin.ModelAdmin):
    list_display = (
        "vendor",
        "total",
        "tax",
        "date",
        "category",
        "account",
        "id",
    )


@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    list_display = ("name", "number", "id")
